function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

var page_name = getUrlVars()["p"];
var mode = getUrlVars()["mode"];

if (!page_name) {
    page_name = "home"
}